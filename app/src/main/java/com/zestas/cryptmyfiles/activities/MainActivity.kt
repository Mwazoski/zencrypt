package com.zestas.cryptmyfiles.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.withStarted
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import com.zestas.cryptmyfiles.fragments.DecryptedViewFragment
import com.zestas.cryptmyfiles.fragments.EncryptedViewFragment
import com.zestas.cryptmyfiles.fragments.PasswordAnalyzerFragment
import com.zestas.cryptmyfiles.fragments.SettingsFragment
import com.zestas.cryptmyfiles.helpers.*
import com.zestas.cryptmyfiles.helpers.ExitHelper.Companion.exit
import com.zestas.cryptmyfiles.helpers.autodeletefiles.ZencryptLifecycleObserver
import com.zestas.cryptmyfiles.helpers.ui.FabHelper
import com.zestas.cryptmyfiles.helpers.ui.SnackBarHelper
import com.zestas.cryptmyfiles.helpers.ui.ToolbarHelper
import com.zestas.cryptmyfiles.viewModels.SharedViewModel
import dev.skomlach.biometric.compat.BiometricPromptCompat
import kotlinx.coroutines.*
import com.zestas.cryptmyfiles.ZenCrypt

class MainActivity : AppCompatActivity() {

    private val bottomNavBar by lazy { findViewById<ChipNavigationBar>(R.id.bottom_menu) }
    private val fab by lazy { findViewById<ExtendedFloatingActionButton>(R.id.fab) }
    private val fabScrim by lazy { findViewById<FrameLayout>(R.id.fab_scrim) }
    private val toolbar by lazy { findViewById<MaterialToolbar>(R.id.toolbar) }
    private lateinit var searchMenuItem: MenuItem
    private var sharedViewModel: SharedViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(if (ZenCryptSettingsModel.darkTheme.value) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        sharedViewModel = (application as ZenCrypt).sharedViewModel
        setContentView(R.layout.activity_main)
        BiometricPromptCompat.init(null)

        bottomNavBar.setItemSelected(R.id.encrypted)

        init()

        if(savedInstanceState == null)
            sharedViewModel?.replaceFragmentWithDelay(EncryptedViewFragment(), 0)
        else if(themeChanged) {
            fab.hide()
            searchMenuItem.isVisible = false
        }
    }

    private fun init() {
        initObservers()
        ToolbarHelper.makeTitle(this, toolbar, getString(R.string.app_name))
        searchMenuItem = toolbar.menu.findItem(R.id.search_files)
        FabHelper.init(this)
        com.pvryan.easycrypt.Constants.setCustomIterationNumber(ZenCryptUtils.getIterations())

        if (UpdateMigrationHelper.checkForFingerprintAuthStorageMigration())
            UpdateMigrationHelper.performFingerprintAuthStorageMigration(this)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                PermissionHelper.checkPermissions(this@MainActivity)
                ChangelogHelper.showChangelogOnUpdate(this@MainActivity)
            }
        }

        bottomNavBar.setOnItemSelectedListener { id ->
            when (id) {
                R.id.encrypted -> {
                    sharedViewModel?.replaceFragmentWithDelay(EncryptedViewFragment())
                    fab.show()
                    searchMenuItem.isVisible = true
                }
                R.id.decrypted -> {
                    sharedViewModel?.replaceFragmentWithDelay(DecryptedViewFragment())
                    fab.show()
                    searchMenuItem.isVisible = true
                }
                R.id.pass_analysis -> {
                    sharedViewModel?.replaceFragmentWithDelay(PasswordAnalyzerFragment())
                    if (fabScrim.isVisible)
                        fabScrim.performClick()
                    fab.hide()
                    searchMenuItem.isVisible = false
                }
                R.id.settings -> {
                    sharedViewModel?.replaceFragmentWithDelay(SettingsFragment())
                    if (fabScrim.isVisible)
                        fabScrim.performClick()
                    fab.hide()
                    searchMenuItem.isVisible = false
                }
                //else -> R.color.white to ""
            }
        }

        //api 33 implementation of onBackPressed which is now deprecated!
        onBackPressedDispatcher.addCallback(this /* lifecycle owner */, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (searchMenuItem.isActionViewExpanded)
                    searchMenuItem.collapseActionView()
                else if (fabScrim.isVisible)
                    fabScrim.performClick()
                else if (bottomNavBar.getSelectedItemId() != R.id.encrypted) {
//                      replaceFragmentWithDelay(EncryptedViewFragment())
                    bottomNavBar.setItemSelected(R.id.encrypted)
                }
                else {
                    exit(this@MainActivity)
                }
            }
        })

        //Check for received/shared Uri
        if (intent.extras != null && intent.extras!!.containsKey(ZenCryptUtils.REQUEST_CODE)) {
            val dialog = AlertDialog.Builder(this, R.style.AlertDialogCustom)
            dialog.setTitle(getString(R.string.file_received))
            dialog.setMessage(getString(R.string.please_choose_what_you_want_to_do))
            dialog.setNegativeButton("Decrypt") { dia, _ ->
                val actionIntent = Intent(this, ActionActivity::class.java)
                actionIntent.putExtra(ZenCryptUtils.REQUEST_CODE, ZenCryptUtils.FROM_RECEIVED_URI)
                actionIntent.putExtra(ZenCryptUtils.ACTION_CODE, ZenCryptUtils.ACTION_DECRYPT)
                actionIntent.data = intent.data
                startActivity(actionIntent)
                dia.dismiss()
            }
            dialog.setPositiveButton("Encrypt") { dia, _ ->
                val actionIntent = Intent(this, ActionActivity::class.java)
                actionIntent.putExtra(ZenCryptUtils.REQUEST_CODE, ZenCryptUtils.FROM_RECEIVED_URI)
                actionIntent.putExtra(ZenCryptUtils.ACTION_CODE, ZenCryptUtils.ACTION_ENCRYPT)
                actionIntent.data = intent.data
                startActivity(actionIntent)
                dia.dismiss()
            }
            if (!this.isFinishing)
                dialog.show()
        }
    }

    private fun initObservers() {
        if (ZenCryptSettingsModel.delete_unencrypted_on_exit.value)
            lifecycle.addObserver(ZencryptLifecycleObserver(this))

        sharedViewModel?.fragmentTransactionEvent?.observe(this) { event ->
            lifecycleScope.launch {
                // very important to use launchWhenStarted {...} here. If we were to use launch {...}
                // the isStateSaved variable will be false and the transaction is not safe to commit,
                // resulting in app crash due to illegalStateException. launchWhenStarted ensures that the
                // state is saved and the transaction is safe to commit!
                delay(event.delay)
                withStarted {
                    val backStateName = event.fragment.javaClass.name
                    val fragmentPopped = supportFragmentManager.popBackStackImmediate(backStateName, 0)

                    if (!fragmentPopped && supportFragmentManager.findFragmentByTag(backStateName) == null) {
                        val fragmentTransaction = supportFragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.container, event.fragment)
                        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        fragmentTransaction.addToBackStack(backStateName)
                        fragmentTransaction.commit()
                    }
                }
            }
        }

        sharedViewModel?.selectedItemId?.observe(this) { itemId ->
            if (itemId != null) {
                if (bottomNavBar.getSelectedItemId() != itemId)
                    bottomNavBar.setItemSelected(itemId)
                else {
                    val newFragment = when (supportFragmentManager.findFragmentById(R.id.container)) {
                        is EncryptedViewFragment -> EncryptedViewFragment()
                        is DecryptedViewFragment -> DecryptedViewFragment()
                        else -> null
                    }
                    if (newFragment != null) {
                        sharedViewModel?.replaceFragmentWithDelay(newFragment, 0)
                    }
                }
                sharedViewModel?.selectedItemId?.postValue(null)
            }
        }

        sharedViewModel?.infoMessage?.observe(this) { event ->
            event?.getContentIfNotHandled()?.let { message ->
                SnackBarHelper.showSnackBarInfo(this, message)
            }
        }

        sharedViewModel?.checkMessage?.observe(this) { event ->
            event?.getContentIfNotHandled()?.let { message ->
                SnackBarHelper.showSnackBarCheck(this, message)
            }
        }

        sharedViewModel?.errorMessage?.observe(this) { event ->
            event?.getContentIfNotHandled()?.let { message ->
                SnackBarHelper.showSnackBarError(this, message)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        sharedViewModel?.clearData()
        sharedViewModel = null
    }

    companion object {
        private var themeChanged: Boolean = false
        fun themeChanged() {
            themeChanged = true
        }
    }
}