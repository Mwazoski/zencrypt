package com.zestas.cryptmyfiles.helpers

import android.annotation.SuppressLint
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyInfo
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import java.nio.charset.Charset
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.security.PrivateKey
import java.security.PublicKey
import java.security.UnrecoverableEntryException
import java.security.UnrecoverableKeyException
import java.security.cert.CertificateException
import java.security.spec.AlgorithmParameterSpec
import java.security.spec.InvalidKeySpecException
import java.security.spec.MGF1ParameterSpec
import java.util.Calendar
import java.util.GregorianCalendar
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource


class SecureStorageHelper {
    private var secureStorage: SecureStorageImpl = SecureStorageHelper_SDK18()

    init {
        var isInitialized = false
        try {
            isInitialized = secureStorage.init()
        } catch (ex: Exception) {
            Log.e("Zen", "SecureStorage initialisation error:" + ex.message, ex)
        }
        if (!isInitialized) {
            Log.d("Zen", "SecureStorage not initialized")
        }
    }

    fun storeSecretP(activity: AppCompatActivity, data: ByteArray) {
        secureStorage.storeSecretP(activity, data)
    }

    fun getSecretP(): String? {
        return secureStorage.getSecretP()?.toString(Charset.defaultCharset())
    }

    private interface SecureStorageImpl {
        fun init(): Boolean
        fun storeSecretP(activity: AppCompatActivity, data: ByteArray)
        fun getSecretP(): ByteArray?
    }

    private class SecureStorageHelper_SDK18 : SecureStorageImpl {
        private lateinit var alias: String


        @Suppress("deprecation")
        @SuppressLint("NewApi", "TrulyRandom")
        override fun init(): Boolean {
            alias = ZenCryptUtils.FINGERPRINT_KEYSTORE_ALIAS
            Log.d("Zen", "Started SecureStorage init()")
            val ks: KeyStore?
            try {
                ks = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)

                //Use null to load Keystore with default parameters.
                ks.load(null)
                // Check if Private and Public already keys exists. If so we don't need to generate them again
                val privkey = (ks.getEntry(alias,null) as KeyStore.PrivateKeyEntry?)?.privateKey

                if (privkey != null && ks.getCertificate(alias) != null) {
                    val publicKey = ks.getCertificate(alias).publicKey
                    if (publicKey != null) {
                        // All keys are available.
                        return true
                    }
                }
            } catch (ex: Exception) {
                Log.d("Zen", "failed, $ex")
                return false
            }

            // Create a start and end time, for the validity range of the key pair that's about to be
            // generated.
//            val start: Calendar = GregorianCalendar()
            val end: Calendar = GregorianCalendar()
            end.add(Calendar.YEAR, 10)

            // Specify the parameters object which will be passed to KeyPairGenerator
            val spec: AlgorithmParameterSpec
            spec = KeyGenParameterSpec.Builder(
                alias,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setDigests(KeyProperties.DIGEST_SHA256)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                .setKeySize(2048)
                .build()
            // Initialize a KeyPair generator using the the intended algorithm (RSA
            // and the KeyStore)
            val kpGenerator: KeyPairGenerator
            try {
                kpGenerator = KeyPairGenerator.getInstance(
                    KEY_ALGORITHM_RSA,
                    KEYSTORE_PROVIDER_ANDROID_KEYSTORE
                )
                kpGenerator.initialize(spec)
                // Generate private/public keys
                kpGenerator.generateKeyPair()
            } catch (e: NoSuchAlgorithmException) {
                try {
                    ks?.deleteEntry(alias)
                } catch (e1: Exception) {
                    // Just ignore any errors here
                }
            } catch (e: InvalidAlgorithmParameterException) {
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) {
                }
            } catch (e: NoSuchProviderException) {
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) {
                }
            }

            // Check if device support Hardware-backed keystore
            try {
                val privateKey = ks.getKey(alias, null) as PrivateKey
//                KeyChain.isBoundKeyAlgorithm(KeyProperties.KEY_ALGORITHM_RSA)
                val keyFactory = KeyFactory.getInstance(privateKey.algorithm, "AndroidKeyStore")
                val keyInfo = keyFactory.getKeySpec(
                    privateKey,
                    KeyInfo::class.java
                )
                val isHardwareBackedKeystoreSupported: Boolean = keyInfo.isInsideSecureHardware

                Log.d(
                    "Zen",
                    "Hardware-Backed Keystore Supported: $isHardwareBackedKeystoreSupported"
                )
            } catch (_: KeyStoreException) {
            } catch (_: NoSuchAlgorithmException) {
            } catch (_: UnrecoverableKeyException) {
            } catch (_: InvalidKeySpecException) {
            } catch (_: NoSuchProviderException) {
            }
            return true
        }

        override fun storeSecretP(activity: AppCompatActivity, data: ByteArray) {
            var ks: KeyStore? = null
            try {
                ks = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)
                ks.load(null)
                if (ks.getCertificate(alias) == null) return
                val publicKey = ks.getCertificate(alias).publicKey
                if (publicKey == null) {
                    Log.d("Zen", "Error: Public key was not found in Keystore")
                    return
                }
                val value = encrypt(publicKey, data)
                activity.lifecycleScope.launch(Dispatchers.IO) {
                    ZenCryptSettingsModel.fingerprint_auth_secret.update(value)
                }
            } catch (e: NoSuchAlgorithmException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) {
                    // Just ignore any errors here
                }
            } catch (e: InvalidKeyException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: NoSuchPaddingException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: IllegalBlockSizeException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: BadPaddingException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: NoSuchProviderException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: InvalidKeySpecException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: KeyStoreException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: CertificateException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: IOException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            }
        }

        override fun getSecretP(): ByteArray? {
            var ks: KeyStore? = null
            try {
                ks = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)
                ks.load(null)
                val privkey =
                    (ks.getEntry(alias, null) as KeyStore.PrivateKeyEntry?)?.privateKey ?: return null
                return decrypt(privkey, ZenCryptSettingsModel.fingerprint_auth_secret.value)
            } catch (e: KeyStoreException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) {
                    // Just ignore any errors here
                }
            } catch (e: NoSuchAlgorithmException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: CertificateException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: IOException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: UnrecoverableEntryException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: InvalidKeyException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: NoSuchPaddingException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: IllegalBlockSizeException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: BadPaddingException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            } catch (e: NoSuchProviderException) {
                Log.d("Zen", "error: $e")
                try {
                    ks?.deleteEntry(alias)
                } catch (_: Exception) { }
            }
            Log.d("Zen", "null")
            return null
        }

        companion object {
            private const val KEY_ALGORITHM_RSA = "RSA"
            private const val KEYSTORE_PROVIDER_ANDROID_KEYSTORE = "AndroidKeyStore"
            private const val RSA_ECB_OAEPSHA_MGF1_PADDING = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"

            @SuppressLint("TrulyRandom")
            @Throws(
                NoSuchAlgorithmException::class,
                NoSuchPaddingException::class,
                InvalidKeyException::class,
                IllegalBlockSizeException::class,
                BadPaddingException::class,
                NoSuchProviderException::class,
                InvalidKeySpecException::class
            )
            private fun encrypt(encryptionKey: PublicKey, data: ByteArray): String {
                val sp = OAEPParameterSpec(
                    "SHA-256",
                    "MGF1",
                    MGF1ParameterSpec("SHA-1"),
                    PSource.PSpecified.DEFAULT
                )
                val cipher = Cipher.getInstance(RSA_ECB_OAEPSHA_MGF1_PADDING)
                cipher.init(Cipher.ENCRYPT_MODE, encryptionKey, sp)
                val encrypted = cipher.doFinal(data)
                return Base64.encodeToString(encrypted, Base64.DEFAULT)
            }

            @Throws(
                NoSuchAlgorithmException::class,
                NoSuchPaddingException::class,
                InvalidKeyException::class,
                IllegalBlockSizeException::class,
                BadPaddingException::class,
                NoSuchProviderException::class
            )
            private fun decrypt(decryptionKey: PrivateKey, encryptedData: String?): ByteArray? {
                if (encryptedData == null) return null
                val sp = OAEPParameterSpec(
                    "SHA-256",
                    "MGF1",
                    MGF1ParameterSpec("SHA-1"),
                    PSource.PSpecified.DEFAULT
                )
                val encryptedBuffer = Base64.decode(encryptedData, Base64.DEFAULT)
                val cipher = Cipher.getInstance(RSA_ECB_OAEPSHA_MGF1_PADDING)
                cipher.init(Cipher.DECRYPT_MODE, decryptionKey, sp)
                return cipher.doFinal(encryptedBuffer)
            }
        }
    }
}
