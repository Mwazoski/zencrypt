package com.zestas.cryptmyfiles.helpers

import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.withStarted
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.ZenCrypt
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.fragments.DecryptedViewFragment
import com.zestas.cryptmyfiles.fragments.EncryptedViewFragment
import com.zestas.cryptmyfiles.helpers.ui.SnackBarHelper
import kotlinx.coroutines.launch
import java.io.File


class FileActionsHelper {
    companion object {
        fun showFileDeleteConfirmDialog(activity: AppCompatActivity, documentFile: DocumentFile, replaceWith: Int) {
            val sharedViewModel = (activity.application as ZenCrypt).sharedViewModel
            val name = documentFile.name
            val title = activity.getString(R.string.confirm_deletion)
            val dialog = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
            dialog.setTitle(title)
            dialog.setMessage("Are you sure you want to delete:\n$name")
            dialog.setNegativeButton("No") { dia, _ -> dia.cancel() }
            dialog.setPositiveButton("Yes") { dia, _ ->
                if (documentFile.exists() && documentFile.canWrite()) {
                    documentFile.delete()
                    sharedViewModel.replaceFragmentWithDelay(
                        if (replaceWith == ZenCryptUtils.REPLACE_WITH_ENCRYPTED) EncryptedViewFragment()
                        else DecryptedViewFragment(), 0
                    )
                    SnackBarHelper.showSnackBarCheck(activity, "Successfully deleted $name")
                } else {
                    SnackBarHelper.showSnackBarError(activity, "Something went wrong. Could not delete $name")
                }
                dia.cancel()
            }
            if (!activity.isFinishing) {
                dialog.show()
            }
        }

        fun showFileDeleteConfirmDialog(activity: AppCompatActivity, file: File, replaceWith: Int) {
            val sharedViewModel = (activity.application as ZenCrypt).sharedViewModel
            val name = file.name
            val title = activity.getString(R.string.confirm_deletion)
            val dialog = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
            dialog.setTitle(title)
            dialog.setMessage("Are you sure you want to delete:\n$name")
            dialog.setNegativeButton("No") { dia, _ -> dia.cancel() }
            dialog.setPositiveButton("Yes") { dia, _ ->
                if (file.exists() && file.canWrite()) {
                    file.delete()
                    sharedViewModel.replaceFragmentWithDelay(
                        if (replaceWith == ZenCryptUtils.REPLACE_WITH_ENCRYPTED) EncryptedViewFragment()
                        else DecryptedViewFragment(), 0
                    )
                    SnackBarHelper.showSnackBarCheck(activity, "Successfully deleted $name")
                } else {
                    SnackBarHelper.showSnackBarError(activity, "Something went wrong. Could not delete $name")
                }
                dia.cancel()
            }
            if (!activity.isFinishing) {
                dialog.show()
            }
        }

        fun showDeleteAllFilesConfirmDialog(activity: AppCompatActivity) {
            val progressDialog: android.app.AlertDialog
            val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(activity)
            builder.setCancelable(false)
            builder.setView(R.layout.indeterminate_progress_circular)
            progressDialog = builder.create()
            val title = activity.getString(R.string.confirm_deletion)
            val dialog = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
            dialog.setTitle(title)
            dialog.setMessage("Are you sure you want to delete all files?")
            dialog.setNegativeButton("No") { dia, _ -> dia.cancel() }
            dialog.setPositiveButton("Yes") { dia, _ ->
                progressDialog.show()
                activity.lifecycleScope.launch {
                    activity.withStarted {
                        if (ZenCryptUtils.isUsingCustomDirectory()) {
                            val encryptedFilesDir =
                                ZenCryptUtils.encryptedFilesDirExternal(activity)
                            val decryptedFilesDir =
                                ZenCryptUtils.decryptedFilesDirExternal(activity)

                            for (file in encryptedFilesDir.listFiles()) {
                                if (!file.isDirectory) {
                                    if (!file.delete())
                                        Log.d("ZenInfo", "File deletion " + file.name + " FAILED")
                                }
                            }
                            for (file in decryptedFilesDir.listFiles()) {
                                if (!file.isDirectory) {
                                    if (!file.delete())
                                        Log.d("ZenInfo", "File deletion " + file.name + " FAILED")
                                }
                            }
                        }
                        else {
                            val encryptedFilesDir =
                                ZenCryptUtils.encryptedFilesDirInternal(activity)
                            val decryptedFilesDir =
                                ZenCryptUtils.encryptedFilesDirInternal(activity)

                            for (file in encryptedFilesDir.listFiles()!!) {
                                if (!file.isDirectory) {
                                    if (!file.delete())
                                        Log.d("ZenInfo", "File deletion " + file.name + " FAILED")
                                }
                            }
                            for (file in decryptedFilesDir.listFiles()!!) {
                                if (!file.isDirectory) {
                                    if (!file.delete())
                                        Log.d("ZenInfo", "File deletion " + file.name + " FAILED")
                                }
                            }
                        }
                    }
                    dia.cancel()
                    if (progressDialog.isShowing) progressDialog.dismiss()
                }
            }
            if (!activity.isFinishing) {
                dialog.show()
            }
        }

        fun showFileRenameDialog(activity: AppCompatActivity, documentFile: DocumentFile, replaceWith: Int) {
            val sharedViewModel = (activity.application as ZenCrypt).sharedViewModel
            val oldName = documentFile.name!!
            val textInputLayout =
                TextInputLayout(
                    ContextThemeWrapper(
                        activity,
                        R.style.Widget_Material3_TextInputLayout_OutlinedBox
                    )
                )
            textInputLayout.setPadding(
                activity.resources.getDimensionPixelOffset(R.dimen.dp_19),
                activity.resources.getDimensionPixelOffset(R.dimen.dp_19),
                activity.resources.getDimensionPixelOffset(R.dimen.dp_19),
                0
            )
            textInputLayout.startIconDrawable =
                ContextCompat.getDrawable(activity, R.drawable.pencil)
            textInputLayout.setStartIconTintList(
                ContextCompat.getColorStateList(
                    textInputLayout.context,
                    R.color.text_input_color_selector
                )
            )
//            textInputLayout.boxBackgroundColor =
//                MaterialColors.getColor(textInputLayout, R.attr.navMenuColor)
            textInputLayout.boxBackgroundMode = TextInputLayout.BOX_BACKGROUND_OUTLINE
            textInputLayout.setBoxCornerRadii(
                14F,
                14F,
                14F,
                14F
            )
            val input = TextInputEditText(textInputLayout.context)
            input.textSize = 16F
            textInputLayout.hint = oldName
            textInputLayout.addView(input)
            val renameString = activity.getString(R.string.rename)
            val message = activity.getString(R.string.please_enter_file_name_without_extension)

            val alert = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
                .setTitle(renameString)
                .setView(textInputLayout)
                .setMessage(message)
                .setPositiveButton(renameString) { dialog, _ ->
                    var newName: String = ""
                    val dir =
                        if (replaceWith == ZenCryptUtils.REPLACE_WITH_ENCRYPTED)
                            ZenCryptUtils.encryptedFilesDirExternal(activity)
                        else ZenCryptUtils.decryptedFilesDirExternal(activity)
                    if (oldName.matches(".*\\..*\\..*".toRegex())) {
                        val ix1 = oldName.lastIndexOf('.')
                        val ix2 = oldName.lastIndexOf('.', ix1 - 1)
                        newName = input.text.toString() + oldName.substring(ix2, ix1) + oldName.substring(ix1, oldName.length)
                    } else {
                        val ix1 = oldName.lastIndexOf('.')
                        newName = input.text.toString() + oldName.substring(ix1, oldName.length)
                    }
                    if (dir.findFile(newName) != null)
                        SnackBarHelper.showSnackBarError(activity, "File already exists!")
                    else {
                        val success = documentFile.renameTo(newName)
                        if (success)
                            SnackBarHelper.showSnackBarCheck(activity, "Successfully renamed file!")
                    }
                    sharedViewModel.replaceFragmentWithDelay(
                        if (replaceWith == ZenCryptUtils.REPLACE_WITH_ENCRYPTED) EncryptedViewFragment()
                        else DecryptedViewFragment(), 0
                    )
                    dialog.cancel()
                }
                .setNeutralButton(activity.getString(android.R.string.cancel)) { dialog, _ ->
                    dialog.cancel()
                }.create()

            if (!activity.isFinishing) {
                alert.show()
            }
        }

        fun showFileRenameDialog(activity: AppCompatActivity, file: File, replaceWith: Int) {
            val sharedViewModel = (activity.application as ZenCrypt).sharedViewModel
            val oldName = file.name
            val textInputLayout =
                TextInputLayout(
                    ContextThemeWrapper(
                        activity,
                        R.style.Widget_Material3_TextInputLayout_OutlinedBox
                    )
                )
            textInputLayout.setPadding(
                activity.resources.getDimensionPixelOffset(R.dimen.dp_19),
                activity.resources.getDimensionPixelOffset(R.dimen.dp_19),
                activity.resources.getDimensionPixelOffset(R.dimen.dp_19),
                0
            )
            textInputLayout.startIconDrawable =
                ContextCompat.getDrawable(activity, R.drawable.pencil)
            textInputLayout.setStartIconTintList(
                ContextCompat.getColorStateList(
                    textInputLayout.context,
                    R.color.text_input_color_selector
                )
            )
//            textInputLayout.boxBackgroundColor =
//                MaterialColors.getColor(textInputLayout, R.attr.navMenuColor)
            textInputLayout.boxBackgroundMode = TextInputLayout.BOX_BACKGROUND_OUTLINE
            textInputLayout.setBoxCornerRadii(
                14F,
                14F,
                14F,
                14F
            )
            val input = TextInputEditText(textInputLayout.context)
            input.textSize = 16F
            textInputLayout.hint = oldName
            textInputLayout.addView(input)
            val renameString = activity.getString(R.string.rename)
            val message = activity.getString(R.string.please_enter_file_name_without_extension)

            val alert = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
                .setTitle(renameString)
                .setView(textInputLayout)
                .setMessage(message)
                .setPositiveButton(renameString) { dialog, _ ->
                    val newFile: File
                    val path =
                        if (replaceWith == ZenCryptUtils.REPLACE_WITH_ENCRYPTED)
                            ZenCryptUtils.encryptedFilesDirInternal(activity).absolutePath
                        else ZenCryptUtils.decryptedFilesDirInternal(activity).absolutePath
                    if (oldName.matches(".*\\..*\\..*".toRegex())) {
                        val ix1 = oldName.lastIndexOf('.')
                        val ix2 = oldName.lastIndexOf('.', ix1 - 1)
                        val dest = path +
                                File.separator +
                                input.text +
                                oldName.substring(ix2, ix1) +
                                oldName.substring(ix1, oldName.length)
                        newFile = File(dest)
                    } else {
                        val ix1 = oldName.lastIndexOf('.')
                        val dest = path +
                                File.separator +
                                input.text +
                                oldName.substring(ix1, oldName.length)
                        newFile = File(dest)
                    }
                    if (newFile.exists())
                        SnackBarHelper.showSnackBarError(activity, "File already exists!")
                    else {
                        val success = file.renameTo(newFile)
                        if (success)
                            SnackBarHelper.showSnackBarCheck(activity, "Successfully renamed file!")
                    }
                    sharedViewModel.replaceFragmentWithDelay(
                        if (replaceWith == ZenCryptUtils.REPLACE_WITH_ENCRYPTED) EncryptedViewFragment()
                        else DecryptedViewFragment(), 0
                    )
                    dialog.cancel()
                }
                .setNeutralButton(activity.getString(android.R.string.cancel)) { dialog, _ ->
                    dialog.cancel()
                }.create()

            if (!activity.isFinishing) {
                alert.show()
            }
        }
    }
}