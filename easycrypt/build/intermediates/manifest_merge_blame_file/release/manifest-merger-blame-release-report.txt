1<?xml version="1.0" encoding="utf-8"?>
2<!--
3    Copyright 2018 Priyank Vasa
4    Licensed under the Apache License, Version 2.0 (the "License");
5    you may not use this file except in compliance with the License.
6    You may obtain a copy of the License at
7
8    http://www.apache.org/licenses/LICENSE-2.0
9
10    Unless required by applicable law or agreed to in writing, software
11    distributed under the License is distributed on an "AS IS" BASIS,
12    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
13    See the License for the specific language governing permissions and
14    limitations under the License.
15-->
16<manifest xmlns:android="http://schemas.android.com/apk/res/android"
17    package="com.pvryan.easycrypt" >
18
19    <uses-sdk android:minSdkVersion="23" />
20
21    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
21-->/home/helios/AndroidProjects/ZenCrypt/easycrypt/src/main/AndroidManifest.xml:18:5-80
21-->/home/helios/AndroidProjects/ZenCrypt/easycrypt/src/main/AndroidManifest.xml:18:22-77
22    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
22-->/home/helios/AndroidProjects/ZenCrypt/easycrypt/src/main/AndroidManifest.xml:19:5-81
22-->/home/helios/AndroidProjects/ZenCrypt/easycrypt/src/main/AndroidManifest.xml:19:22-78
23    <uses-permission android:name="android.permission.INTERNET" />
23-->/home/helios/AndroidProjects/ZenCrypt/easycrypt/src/main/AndroidManifest.xml:20:5-67
23-->/home/helios/AndroidProjects/ZenCrypt/easycrypt/src/main/AndroidManifest.xml:20:22-64
24
25</manifest>
